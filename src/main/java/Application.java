import data.dao.StudentDaoImpl;
import service.SortingServiceImpl;
import ui.model.StudentModel;
import ui.model.StudentModelImpl;
import ui.presenter.StudentPresenter;
import ui.presenter.StudentPresenterImpl;
import ui.view.StudentViewImpl;

public class Application {

    public static void main(String [] args){
        StudentModel model = new StudentModelImpl(new SortingServiceImpl(), new StudentDaoImpl());
        StudentPresenter presenter = new StudentPresenterImpl();
        StudentViewImpl frame = new StudentViewImpl(presenter);
        presenter.setView(frame);
        presenter.setModel(model);
    }
}
