package utils.comparator;

import data.model.Student;

import java.util.Comparator;

public class StudentGradeComparator implements Comparator<Student> {
    public int compare(Student o1, Student o2) {
        return Float.compare(o1.getGrade(), o2.getGrade());
    }
}
