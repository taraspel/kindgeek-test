package utils.sorting;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface SortingStrategy {
    List<Object> sort(List data, Comparator comparator);
}
