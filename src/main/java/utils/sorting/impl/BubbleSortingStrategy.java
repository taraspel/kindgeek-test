package utils.sorting.impl;

import utils.sorting.SortingStrategy;

import java.util.Comparator;
import java.util.List;

public class BubbleSortingStrategy implements SortingStrategy {
    public List<Object> sort(List data, Comparator comparator) {
        int i = 0;
        int length = data.size();
        boolean swapNeeded = true;
        while (i < length - 1 && swapNeeded) {
            swapNeeded = false;
            for (int j = 1; j < length - i; j++) {
                if (comparator.compare(data.get(j - 1), data.get(j)) > 0) {
                    Object temp = data.get(j - 1);
                    data.set(j - 1, data.get(j));
                    data.set(j, temp);
                    swapNeeded = true;
                }
            }
            if (!swapNeeded) {
                break;
            }
            i++;
        }
        return null;
    }
}
