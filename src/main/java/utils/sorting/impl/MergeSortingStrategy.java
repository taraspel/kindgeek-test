package utils.sorting.impl;

import utils.sorting.SortingStrategy;

import java.util.Comparator;
import java.util.List;

public class MergeSortingStrategy implements SortingStrategy {
    List values;
    private Object[] helper;
    private int number;

    @Override
    public List<Object> sort(List data, Comparator comparator) {
        this.values = data;
        number = values.size();
        this.helper = new Object[number];
        mergesort(0, number - 1, comparator);
        return values;
    }

    private void mergesort(int low, int high, Comparator comparator) {
        if (low < high) {
            int middle = low + (high - low) / 2;
            mergesort(low, middle, comparator);
            mergesort(middle + 1, high, comparator);
            merge(low, middle, high, comparator);
        }
    }

    private void merge(int low, int middle, int high, Comparator comparator) {

        for (int i = low; i <= high; i++) {
            helper[i] = values.get(i);
        }
        int i = low;
        int j = middle + 1;
        int k = low;

        while (i <= middle && j <= high) {
            if (comparator.compare(helper[i], helper[j]) <= 0) {
                values.set(k, helper[i]);
                i++;
            } else {
                values.set(k, helper[j]);
                j++;
            }
            k++;
        }
        while (i <= middle) {
            values.set(k, helper[i]);
            k++;
            i++;
        }
    }
}