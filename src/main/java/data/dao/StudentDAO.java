package data.dao;

import data.model.Student;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface StudentDAO {
    List<Student> getStudents(File file) throws IOException;

    void saveStudents(List<Student> students, File file) throws FileNotFoundException;
}
