package data.dao;

import data.model.Student;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDAO {
    public List<Student> getStudents(File file) throws IOException {
        List<Student> students = new ArrayList<Student>();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = reader.readLine();
        while (line != null) {
            Student student = parseStudent(line);
            if (student != null) {
                students.add(student);
            }
            line = reader.readLine();
        }
        return students;
    }

    public void saveStudents(List<Student> students, File file) throws FileNotFoundException {
        StringBuilder textData = new StringBuilder();
        for(Student student: students){
            textData.append(encodeStudent(student));
            textData.append(System.lineSeparator());
        }
        try (PrintWriter out = new PrintWriter(file)) {
            out.println(textData);
        }
    }

    private String encodeStudent(Student student){
        return student.getName() + "," + student.getGrade();
    }

    private Student parseStudent(String entry) {
        String[] nameGrade = entry.split(",");
        if (nameGrade.length == 2) {
            Student student = new Student();
            student.setName(nameGrade[0]);
            try {
                student.setGrade(Float.parseFloat(nameGrade[1]));
            } catch (NumberFormatException e) {
                return null;
            }
            return student;
        } else {
            return null;
        }
    }
}
