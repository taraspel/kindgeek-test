package service;

import data.model.Student;
import utils.sorting.SortingStrategy;

import java.util.List;

public interface SortingService {
    void setSortingStrategy(SortingStrategy sortingStrategy);
    void sortByName(List<Student> students);
    void sortByGrade(List<Student> students);
}
