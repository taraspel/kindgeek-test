package service;

import data.model.Student;
import utils.comparator.StudentGradeComparator;
import utils.comparator.StudentNameComparator;
import utils.sorting.SortingStrategy;
import utils.sorting.impl.MergeSortingStrategy;

import java.util.List;

public class SortingServiceImpl implements SortingService {

    SortingStrategy sortingStrategy;

    public SortingServiceImpl() {
        sortingStrategy = new MergeSortingStrategy();
    }

    @Override
    public void setSortingStrategy(SortingStrategy sortingStrategy) {
        this.sortingStrategy = sortingStrategy;
    }

    @Override
    public void sortByName(List<Student> students) {
        sortingStrategy.sort(students, new StudentNameComparator());
    }

    @Override
    public void sortByGrade(List<Student> students) {
        sortingStrategy.sort(students, new StudentGradeComparator());
    }
}
