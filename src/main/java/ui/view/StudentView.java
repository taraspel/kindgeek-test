package ui.view;

import data.model.Student;

import java.util.List;

public interface StudentView {
    void setStudents(List<Student> students);
}
