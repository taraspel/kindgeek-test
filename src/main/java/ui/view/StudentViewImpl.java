package ui.view;

import data.model.Student;
import ui.presenter.StudentPresenter;
import utils.Params;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class StudentViewImpl extends JFrame implements StudentView {

    private StudentPresenter presenter;
    private List<Student> students = new ArrayList<Student>();

    private JTable studentsTable;
    private TableModel tableModel;

    public StudentViewImpl(StudentPresenter presenter) throws HeadlessException {
        this.presenter = presenter;
        setSize(500, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());
        initView();
        this.setVisible(true);
    }

    public void setStudents(List<Student> students) {
        this.students = students;
        tableModel.setStudentData(students);
        tableModel.fireTableDataChanged();
    }

    private void initView() {
        JButton button = new JButton(Params.LABEL_OPEN);
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                File file = chooseFile();
                if (file != null) {
                    presenter.getStudents(file);
                }
            }
        });
        add(button, BorderLayout.PAGE_START);
        tableModel = new TableModel();
        studentsTable = new JTable(tableModel);
        studentsTable.getTableHeader().setReorderingAllowed(false);
        studentsTable.getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int col = studentsTable.columnAtPoint(e.getPoint());
                String columnName = studentsTable.getColumnName(col);
                presenter.sortBy(students, columnName);
            }
        });
        add(new JScrollPane(studentsTable), BorderLayout.CENTER);
    }

    private File chooseFile() {
        JFileChooser fileopen = new JFileChooser();
        int ret = fileopen.showDialog(null, Params.LABEL_OPEN);
        if (ret == JFileChooser.APPROVE_OPTION) {
            return fileopen.getSelectedFile();
        }
        return null;
    }

    private class TableModel extends AbstractTableModel {

        private Student[] students;

        public void setStudentData(List<Student> students) {
            this.students = students.toArray(new Student[0]);
        }

        public int getRowCount() {
            if (students != null) {
                return students.length;
            }
            return 0;
        }

        public int getColumnCount() {
            return 2;
        }

        public String getColumnName(int col) {
            switch (col) {
                case 0: {
                    return Params.LABEL_NAME;
                }
                case 1: {
                    return Params.LABEL_GRADE;
                }
                default: {
                    return "";
                }
            }
        }

        public boolean isCellEditable(int row, int col) {
            return false;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            switch (columnIndex) {
                case 0: {
                    return students[rowIndex].getName();
                }
                case 1: {
                    return students[rowIndex].getGrade();
                }
                default: {
                    return "";
                }
            }
        }

    }
}
