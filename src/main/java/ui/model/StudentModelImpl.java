package ui.model;

import data.dao.StudentDAO;
import data.model.Student;
import service.SortingService;
import utils.Params;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class StudentModelImpl implements StudentModel {
    private SortingService sortingService;
    private StudentDAO dao;

    public StudentModelImpl(SortingService sortingService, StudentDAO dao) {
        this.sortingService = sortingService;
        this.dao = dao;
    }

    public List<Student> getStudents(File file) throws IOException {
        List<Student> students = dao.getStudents(file);
        sortingService.sortByName(students);
        return students;
    }

    public void saveStudents(List<Student> students, File file) throws FileNotFoundException {
        dao.saveStudents(students, file);
    }

    public void sortStudents(List<Student> students, String column) {
        if (Params.LABEL_NAME.equals(column)) {
            sortingService.sortByName(students);
        } else if (Params.LABEL_GRADE.equals(column)) {
            sortingService.sortByGrade(students);
        }
    }
}
