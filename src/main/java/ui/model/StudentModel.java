package ui.model;

import data.model.Student;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface StudentModel {
    List<Student> getStudents(File file) throws IOException;

    void saveStudents(List<Student> students, File file) throws FileNotFoundException;

    void sortStudents(List<Student> students, String column);
}
