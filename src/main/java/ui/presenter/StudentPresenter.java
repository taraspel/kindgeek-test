package ui.presenter;

import data.model.Student;
import ui.view.StudentView;
import ui.model.StudentModel;

import java.io.File;
import java.util.List;

public interface StudentPresenter {
    void sortBy(List<Student> students, String columnName);
    void getStudents(File file);
    void saveStudents(List<Student> students, File file);
    void setView(StudentView view);
    void setModel(StudentModel model);
}
