package ui.presenter;

import data.model.Student;
import ui.model.StudentModel;
import ui.view.StudentView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class StudentPresenterImpl implements StudentPresenter
{
    private StudentView view;
    private StudentModel model;

    public void sortBy(List<Student> students, String columnName) {
        model.sortStudents(students, columnName);
        view.setStudents(students);
    }

    public void getStudents(File file) {
        try {
            view.setStudents(model.getStudents(file));
        } catch (IOException e) {
            e.printStackTrace();
//            TODO display error
        }
    }

    public void saveStudents(List<Student> students, File file) {
        try {
            model.saveStudents(students, file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
//            TODO display error
        }
    }

    public void setView(StudentView view) {
        this.view = view;
    }

    public void setModel(StudentModel model) {
        this.model = model;
    }

}
